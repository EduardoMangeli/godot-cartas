extends Node2D

const molde = preload("res://carta_molde.tscn")

var cartas =[
	{
		'imagem' : "res://imagens/img1.jpg",
		'titulo' : 'Arqueira',
		'descri' : 'Um texto grande que não faz sentido estar aqui.'
	},
	{
		'imagem' : "res://imagens/img2.jpg",
		'titulo' : 'Elfa Rosa',
		'descri' : 'Outro texto grande que não faz sentido estar aqui.'
	},
	{
		'imagem' : "res://imagens/img3.jpg",
		'titulo' : 'Com óculos',
		'descri' : 'Mais um texto grande que não faz sentido estar aqui.'
	}
]

var instancia_carta = null
var instancia_imagem = null
var instancia_posicao = 0

func read_data():
	var file = "res://dados.json"
	var conteudo = FileAccess.get_file_as_string(file)
	return JSON.parse_string(conteudo)

func _ready():
	
	for carta in read_data():
		instancia_carta = molde.instantiate()
		instancia_carta.position.x = instancia_posicao
		instancia_posicao += 502
		instancia_imagem = load(carta['imagem'])
		instancia_carta.get_node('painel/imagem').set_texture(instancia_imagem)
		instancia_carta.get_node("painel/titulo").set_text("[center]"+carta['titulo']+"[/center]")
		instancia_carta.get_node('painel/ScrollContainer/Label').set_text(carta['descri'])
		add_child(instancia_carta)
		
		
